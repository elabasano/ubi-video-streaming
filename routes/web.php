<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    return view('welcome');
        return redirect()->to('kalagangan/kalagangan-1');

});
Route::get('kalagangan/{id?}', function($id) {
    $array =  array(
             array('video' => 'kalagangan-1', 'title' => 'Kalagangan Video 1'),
             array('video' => 'kalagangan-2', 'title' => 'Kalagangan Video 2'),
             array('video' => 'kalagangan-3', 'title' => 'Kalagangan Video 3'),
             array('video' => 'kalagangan-4', 'title' => 'Kalagangan Video 4'),
             array('video' => 'kalagangan-5', 'title' => 'Kalagangan Video 5'),
             array('video' => 'kalagangan-6', 'title' => 'Kalagangan Video 6')

    );
    $media = [];
    foreach($array as $row) {
        if ($id === $row['video']) {
            $media = $row;
        }
    }

    return view('welcome', ['media' => $array, 'selected_video' => $media]);
});
Route::get('hd', function () {
    return view('hd');
});
Route::get('720p', function () {
    return view('720');
});
Route::get('/1080p', function () {
    return view('welcome');
});
