<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>UBI Video Streaming</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}"  crossorigin="anonymous">
        <style>
            body {
                background: black !important;
                color: white !important;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @yield('content')
        </div>
    </body>
    <script src="{{ asset('assets/js/jquery-3.3.1.slim.min.js') }}" crossorigin="anonymous"></script>
    <script src="{{ asset('assets/js/popper.min.js') }}" crossorigin="anonymous"></script>
    <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"  crossorigin="anonymous"></script>

</html>
