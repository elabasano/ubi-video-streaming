@extends('tmpl.app')
@section('content')
<br>
<div class="container">
<div class="row">
    <div class="col-sm-2"></div>
        <div class="col-sm-12">
                <video width="100%" height="600px" controls>
                    <source src="{{ asset('assets/media/kalagangan/video/'.$selected_video["video"].'.mp4')}}" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
            <center><h2>{{$selected_video['title']}}</h2></center>

        </div>
    <div class="col-sm-2"></div><br><br>

    <div class="row" style="padding: 20px">
    @foreach($media as $row)

        <div class="col-sm-3"> <a href="{{ url ('kalagangan/'.$row['video'])}}">
        <img src="{{ asset('assets/media/kalagangan/thumb/'.$row["video"].'.jpg')}}" class="img-fluid" alt="..." style="width: 100%">
                <div class="card-body">
                <h5 class="card-title">{{$row['title']}}</h5>
                </div>
            </a>  </div>

    @endforeach
    </div>

</div>
</div>
@endsection
